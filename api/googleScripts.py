
from __future__ import print_function
import httplib2
import os
from api import config

from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage

from apiclient import errors

class googleClient():
    flags = None

    def get_credentials(self):
        """Gets valid user credentials from storage.

        If nothing has been stored, or if the stored credentials are invalid,
        the OAuth2 flow is completed to obtain the new credentials.

        Returns:
            Credentials, the obtained credential.
        """
        home_dir = os.path.dirname(os.path.abspath(__file__))
        credential_dir = os.path.join(home_dir, 'token')
        if not os.path.exists(credential_dir):
            os.makedirs(credential_dir)
        credential_path = os.path.join(credential_dir, config.GOOGLE_CREDENTIAL_FILE)

        store = Storage(credential_path)
        credentials = store.get()
        if not credentials or credentials.invalid:
            flow = client.flow_from_clientsecrets(config.GOOGLE_CLIENT_SECRET_FILE, config.GOOGLE_SCOPES)
            flow.user_agent = config.GOOGLE_APPLICATION_NAME
            if self.flags:
                credentials = tools.run_flow(flow, store, self.flags)
            else: # Needed only for compatibility with Python 2.6
                credentials = tools.run(flow, store)
            print('Storing credentials to ' + credential_path)
        return credentials

    def __init__(self):
        try:

            credentials = self.get_credentials()
            http = credentials.authorize(httplib2.Http())
            self.service = discovery.build('script', 'v1', http=http)

            print('Google script client create!')
        except Exception as exMessage:
            print ('Google client ex: ' + exMessage)
            exit(0)

    def makeNote(self, ss, range, note):
        try:
            request = {"function": 'addNote',
                       "parameters": [
                           ss, range, note
                       ]}

            response = self.service.scripts().run(body=request, scriptId=config.GOOGLE_APPS_SCRIPTS_ID).execute()

            if 'error' in response:
                error = response['error']['details'][0]
                print("Script error message: {0}".format(error['errorMessage']))

                if 'scriptStackTraceElements' in error:
                    print("Script error stacktrace:")
                    for trace in error['scriptStackTraceElements']:
                        print("\t{0}: {1}".format(trace['function'],
                                                  trace['lineNumber']))

        except errors.HttpError as e:
            print(e.content)

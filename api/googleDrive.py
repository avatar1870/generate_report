from __future__ import print_function
import httplib2
import os

from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage
from api import config
from apiclient import errors

try:
    import argparse
    flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
except ImportError:
    flags = None

class googleClient():
    def get_credentials(self):
        home_dir = os.path.dirname(os.path.abspath(__file__))
        credential_dir = os.path.join(home_dir, 'token')
        if not os.path.exists(credential_dir):
            os.makedirs(credential_dir)
        credential_path = os.path.join(credential_dir, config.GOOGLE_CREDENTIAL_FILE)

        store = Storage(credential_path)
        credentials = store.get()
        if not credentials or credentials.invalid:
            flow = client.flow_from_clientsecrets(config.GOOGLE_CLIENT_SECRET_FILE, config.GOOGLE_SCOPES)
            flow.user_agent = config.GOOGLE_APPLICATION_NAME
            if flags:
                credentials = tools.run_flow(flow, store, flags)
            else: # Needed only for compatibility with Python 2.6
                credentials = tools.run(flow, store)
            print('Storing credentials to ' + credential_path)
        return credentials

    def __init__(self):
        try:
            credentials = self.get_credentials()
            http = credentials.authorize(httplib2.Http())
            self.service = discovery.build('drive', 'v2', http=http)
            print ('Google drive client create!')

        except Exception as exMessage:
            print ('Google client ex: ' + exMessage)
            exit(0)


    def deleteFile(self, fileId):
        try:
            self.service.files().delete(fileId=fileId).execute()

            print ('File deleted')
        except errors.HttpError, error:
            print ('deleteFile excetion: %s' % error)

    def moveFile(self, fileId):
        try:
            self.service.files().update(
                fileId=fileId,
                addParents=config.GOOGLE_DESTINATION_SPREADSHEET_FOLDER_ID,
                removeParents='root',
                fields='id, parents').execute()

            print ('File move to folder')
        except errors.HttpError, error:
            print('moveFile exception %s' % error)

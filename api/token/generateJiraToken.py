from oauthlib.oauth1 import SIGNATURE_RSA
from requests_oauthlib import OAuth1Session
from api import config
import service

# The Consumer Key created while setting up the "Incoming Authentication" in
# JIRA for the Application Link.
CONSUMER_SECRET = 'dont_care'
VERIFIER = 'jira_verifier'

# The contents of the rsa.pem file generated (the private RSA key)
RSA_KEY = service.service.read(service.service.getRootFolder() + "/" + config.JIRA_PRIVATE_KEY_PATH)

# The URLs for the JIRA instance
REQUEST_TOKEN_URL = config.JIRA_URL + 'plugins/servlet/oauth/request-token'
AUTHORIZE_URL = config.JIRA_URL + 'plugins/servlet/oauth/authorize'
ACCESS_TOKEN_URL = config.JIRA_URL + 'plugins/servlet/oauth/access-token'

# Step 1: Get a request token
oauth = OAuth1Session(config.JIRA_CONSUMER_KEY, client_secret = CONSUMER_SECRET, signature_method = SIGNATURE_RSA, rsa_key = RSA_KEY)
request_token = oauth.fetch_request_token(REQUEST_TOKEN_URL)

resource_owner_key = request_token['oauth_token'];
resource_owner_secret = request_token['oauth_token_secret'];

print("STEP 1: GET REQUEST TOKEN")
print("  oauth_token={}".format(resource_owner_key))
print("  oauth_token_secret={}".format(resource_owner_secret))
print("\n")

# Step 2: Get the end-user's authorization
print("STEP2: AUTHORIZATION")
print("  Visit to the following URL to provide authorization and sign in:")
link = "  {}?oauth_token={}".format(AUTHORIZE_URL, request_token['oauth_token'])
service.service.openLink(link)
print(link)
print("\n")

raw_input("Press any key to continue...")

oauth = OAuth1Session(config.JIRA_CONSUMER_KEY, client_secret= CONSUMER_SECRET, resource_owner_key=resource_owner_key, resource_owner_secret=resource_owner_secret, verifier=VERIFIER, signature_method=SIGNATURE_RSA, rsa_key=RSA_KEY)

# Step 3: Get the access token
access_token = oauth.fetch_access_token(ACCESS_TOKEN_URL)

print("STEP3: GET ACCESS TOKEN")
print("  access_token={}".format(access_token['oauth_token']))
print("  access_token_secret={}".format(access_token['oauth_token_secret']))
print("\n")

print("STEP4: SAVE ACCESS TOKEN")
print("  Write the token in config!")

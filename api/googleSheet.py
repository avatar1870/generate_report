import sys
reload(sys)
sys.setdefaultencoding("utf-8")
import httplib2
import os
import config

from apiclient import errors

from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage

class googleClient():

    flags = None

    def get_credentials(self):
        try:
            import argparse
            flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
        except ImportError:
            flags = None

        home_dir = os.path.dirname(os.path.abspath(__file__))
        credential_dir = os.path.join(home_dir, 'token')
        if not os.path.exists(credential_dir):
            os.makedirs(credential_dir)
        credential_path = os.path.join(credential_dir, config.GOOGLE_CREDENTIAL_FILE)

        store = Storage(credential_path)
        credentials = store.get()
        if credentials is None or credentials.invalid:
            flow = client.flow_from_clientsecrets(config.GOOGLE_CLIENT_SECRET_FILE, config.GOOGLE_SCOPES)
            flow.user_agent = config.GOOGLE_APPLICATION_NAME
            if flags:
                credentials = tools.run_flow(flow, store, flags)
            else: # Needed only for compatibility with Python 2.6
                credentials = tools.run(flow, store)
            print('Storing credentials to ' + credential_path)
        return credentials

    def __init__(self):
        try:
            credentials = self.get_credentials()
            http = credentials.authorize(httplib2.Http())
            discoveryUrl = ('https://sheets.googleapis.com/$discovery/rest?''version=v4')
            self.service = discovery.build('sheets', 'v4', http=http, discoveryServiceUrl=discoveryUrl)

            print ('Google sheet client create!')
        except Exception as exMessage:
            print ('Google client ex: ' + exMessage)
            exit(0)

    def getSingleValueInSheet(self, spreadsheetId, cellName):
        result = self.service.spreadsheets().values().get(spreadsheetId=spreadsheetId, range=cellName).execute()
        value = result.get('values', [])

        return (value[0][0])

    def getRangeValueInSheet(self, spreadsheetId, rangeName):
        value = None

        try:
            request = self.service.spreadsheets().values().get(spreadsheetId=spreadsheetId, range=rangeName)
            result = request.execute()
            value = result.get('values', [])
        except Exception as exMessage:
            print ('getRangeValueInSheet - Google api exception: ' + str(exMessage))
        return (value)

    def copySheet(self, spreadsheetId, sheetId):
        destinationSpreadsheetId = {
            'destination_spreadsheet_id': spreadsheetId
        }
        request = self.service.spreadsheets().sheets().copyTo(spreadsheetId = spreadsheetId, sheetId = sheetId,
                                                         body = destinationSpreadsheetId)
        response = request.execute()

        newSheetId = response['index']
        return newSheetId

    def updateSingleValue(self, spreadsheetId, cellId, value):
        valueRangeBody = {
            'values': [[value]]
        }
        try:
            request = self.service.spreadsheets().values().update(spreadsheetId = spreadsheetId, range = cellId,
                                                         valueInputOption = 'RAW', body = valueRangeBody)
            response = request.execute()
            print (response['updatedCells'])
        except Exception as exMessage:
            print ('Google api exception: ' + str(exMessage))

    def updateRangeValue(self, spreadsheetId, range, value, dimension = 'ROWS'):
        valueRangeBody = {
            'values': value,
            "majorDimension": dimension,
        }

        try:
            request = self.service.spreadsheets().values().update(spreadsheetId = spreadsheetId, range = range,
                                                              valueInputOption = 'RAW', body = valueRangeBody)
            response = request.execute()
            return str(response['updatedCells'])
        except Exception as exMessage:
            print ('updateRangeValue - Google api exception: ' + str(exMessage))

    def createSheet(self, title):
        spreadsheetBody = {
            "sheets": [],
            "properties": {
                "title": title
            }
        }

        try:
            request = self.service.spreadsheets().create(body = spreadsheetBody)
            response = request.execute()

            return response['spreadsheetId']
        except Exception as exMessage:
            print ('createSheet - Google api exception: ' + str(exMessage))
            exit(0)

    def updateSheetName(self, spreadsheetId, sheetId, newName):
        requestBody = {
        'requests': {
                    "updateSheetProperties": {
                        "properties": {
                            "sheetId": sheetId,
                            "title": newName,
                        },
                        "fields": "title",
                    }
                }
        }

        try:
            request = self.service.spreadsheets().batchUpdate(spreadsheetId = spreadsheetId, body = requestBody)
            response = request.execute()
        except Exception as exMessage:
            print ('updateSheetName - Google api exception: ' + str(exMessage))






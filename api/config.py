# -*- coding: utf-8 -*-
import service
'''Jira data'''
JIRA_URL = 'https://mogilev.awem.by:7990/jira/'
JIRA_ACCESS_TOKEN = 'ZEvTUBGdQjG81maUdrJNASzQMF5J1Bga'
JIRA_ACCESS_TOKEN_SECRET = 'zmfhxz99fRJJAvXA3ls2m6f7OJITEuXK'

JIRA_PRIVATE_KEY_PATH = 'api/token/jira.pem'
JIRA_CONSUMER_KEY = 'hardcoded-consumer'

'''Google project data'''
GOOGLE_SCOPES = 'https://www.googleapis.com/auth/spreadsheets https://www.googleapis.com/auth/drive'
GOOGLE_CLIENT_SECRET_FILE = 'client_secret.json'
GOOGLE_CREDENTIAL_FILE = 'credentials.json'
GOOGLE_APPLICATION_NAME = 'Google Sheets API Python Quickstart'

'''Google apps scripts'''
GOOGLE_APPS_SCRIPTS_ID = 'MeU8lecBCMKKmvIbROCDWGj1ACg1SN8Il'

'''Source data'''
GOOGLE_SOURCE_SPREADSHEATS_ID = '1evrJMPzpI3vilxF8IVw6W94iJFXm3VO9TZbdTIqj7gM'
GOOGLE_SOURCE_SHEET_NAME = 'NewName'
GOOGLE_SOURCE_SHEET_DATA_RANGE = 'A1:O910'
MONTH = '8'
YEAR = '2017'
PROJECT_NAMES = 'PuzzleHeart, COEm iOS, COEm Android'

'''Destination data'''
STAGE_NAME = u'Функциональное тестирование ПО "PROJECT_NAME"'
GOOGLE_DESTINATION_SHEET_HEAD_RANGE = 'A1:H1'
GOOGLE_DESTINATION_SHEET_DATA_RANGE = 'A2:I500'
GOOGLE_DESTINATION_SPREADSHEET_NAME = 'Report ' + YEAR + '.' + service.service.addZeroToMonth(MONTH)
GOOGLE_DESTINATION_SPREADSHEET_URL = 'https://docs.google.com/spreadsheets/d/id/edit#gid=0'
GOOGLE_DESTINATION_SPREADSHEET_FOLDER_ID = '0Byliy7-AWkiRLTd4WE1ENTQzTkE'

'''Match logins'''
#Key = FTOutsource name
#Value = Jira name
matchingLogins = dict()
matchingLogins[u'Алёна Аw'] = u'Алена А'

#Use list for several employees
listLogin = []
listLogin.append(u'Скиба Роман1')
listLogin.append(u'Любимов Владислав1')
matchingLogins[u'GEEKSTONE1'] = listLogin

#For destination docs
headValues = []
headValues.append([u'Задача'])
headValues.append([u'Проект'])
headValues.append([u'Этап'])
headValues.append([u'Версия'])
headValues.append([u'Rate'])
headValues.append([u'H'])
headValues.append([u'Sum $'])
headValues.append([u'Исполнитель'])

'''OTHER'''
CLOSE_AND_COMMENT_ISSUE = False

from enum import Enum

class Field(Enum):
    Rating = 0
    Time = 1
    Version = 2
    Project = 3
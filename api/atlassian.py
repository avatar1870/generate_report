# -*- coding: utf-8 -*-
import service
from api import config

from jira.client import JIRA
from decimal import Decimal

class jiraClient():
    def __init__(self):
        try:
            jiraClient = JIRA(options={'server': config.JIRA_URL}, oauth={
                                        'access_token': config.JIRA_ACCESS_TOKEN,
                                        'access_token_secret': config.JIRA_ACCESS_TOKEN_SECRET,
                                        'consumer_key': config.JIRA_CONSUMER_KEY,
                                        'key_cert': service.service.read(service.service.getRootFolder() + "/" + config.JIRA_PRIVATE_KEY_PATH)
                                    })

            self.jiraClient = jiraClient
            print ('Jira client create!')
        except Exception as exMessage:
            print ('Jira client don`t create ' + str(exMessage))
            exit (0)


    def getSpendTimeOnTask(self, issueId, author = ''):

        resultCheckIssueExist = self.isIssueExist(issueId)

        if resultCheckIssueExist:
            print ('Issue ' + issueId + ' exist')
            issue = self.jiraClient.issue(issueId, fields='summary, reporter, assignee')

            spendTime = 0
            worklogs = self.jiraClient.worklogs(issue.key)

            for worklog in worklogs:

                jiraUpdateTimeAuthor = worklog.updateAuthor.displayName
                jiraUpdateTimeAuthorWithoutUmlauts = worklog.updateAuthor.displayName.replace(u'ё', u'е')
                authorWithoutUmlauts = author.replace(u'ё', u'е')
                isUserInJira = False

                if author in config.matchingLogins:
                    if type(config.matchingLogins[author]) == list:
                        if jiraUpdateTimeAuthor in config.matchingLogins[author]:
                            isUserInJira = True
                    else:
                        if config.matchingLogins[author] == jiraUpdateTimeAuthor:
                            isUserInJira = True

                time = worklog.timeSpentSeconds/3600.0
                if isUserInJira or authorWithoutUmlauts == jiraUpdateTimeAuthorWithoutUmlauts:
                    spendTime += time
                else:
                    print('Logged: ' + jiraUpdateTimeAuthor + ' time:' + str(time) + " --> outsorser: " + author)
            return "%.2f" % round(spendTime, 2)

        else:
            print ('Issue ' + issueId + ' not exist')
            return 0.0

    def isIssueExist(self, issueId):

        if issueId == '':
            return False

        try:
            request = 'id=' + issueId
            search = self.jiraClient.search_issues(request)
            if int(search.total) >= 1:
                return True
            else:
                return False
        except Exception as exMessage:
            print ('Jira exception: ' + str(exMessage))
            return False


    def closeIssue(self, issueId):
        try:
            issue = self.jiraClient.issue(issueId)
            self.jiraClient.transition_issue(issue, '2')
            print ('Close issue: ' + issueId)
        except Exception as exMessage:
            print ('Jira exception: ' + str(exMessage))

    def addComment(self, issueId, comment):
        try:
            issue = self.jiraClient.issue(issueId)
            self.jiraClient.add_comment(issue, comment)
            print ('Add comment ' + comment + ' to issue: ' + issueId)
        except Exception as exMessage:
            print ('Jira exception: ' + str(exMessage))


    def getIssueStatus(self, issueId):
        try:
            issue = self.jiraClient.issue(issueId)
            status = issue.fields.status
            return str(status)
        except Exception as exMessage:
            print ('Jira exception: ' + str(exMessage))
            return 'Closed'

    def getIssueComment(self, issueId):
        try:
            issue = self.jiraClient.issue(issueId)
            comments = issue.fields.comment.comments
            return comments
        except Exception as exMessage:
            print ('Jira exception: ' + str(exMessage))



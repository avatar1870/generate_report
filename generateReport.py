import service

from decimal import Decimal

from api import googleSheet
from api import googleScripts
from api import googleDrive

from api import atlassian
from api import config

from time import sleep
from random import randint

class generateReport():

    spreadSheetId = ''

    def __init__(self):
        self.jiraClient = atlassian.jiraClient()
        self.googleSheetClient = googleSheet.googleClient()
        self.googleScriptClient = googleScripts.googleClient()
        self.googleDriveClient = googleDrive.googleClient()

    def run(self):
        if config.PROJECT_NAMES:
            projectNames = config.PROJECT_NAMES.split(',')

            for currentProject in projectNames:
                print("====================================================================================================")
                print('{:*^100}'.format(currentProject.upper().strip()))
                print("====================================================================================================")
                print ('')

                self.createSourceSpreadSheets(currentProject)
                self.createHead()

                outsourseList = self.getFTOutsourseInfo(currentProject)
                validateOutsourseList = self.validateInfoInJira(outsourseList)

                if validateOutsourseList:
                    self.createReport(validateOutsourseList)
                    self.renameWorkList()
                    self.moveFile()
                    self.openReport()
                else:
                    print ('Empty validateOutsourseList!')
                    print ('Try remove spreadsheet: ' + generateReport.spreadSheetId)
                    self.googleDriveClient.deleteFile(generateReport.spreadSheetId)

        else:
            print ('No project names found!')
            print ('Continues execution is not possible!!!')
            exit (0)

    def getFTOutsourseInfo(self, projectName):
        outsourseInfo = self.googleSheetClient.getRangeValueInSheet(config.GOOGLE_SOURCE_SPREADSHEATS_ID, config.GOOGLE_SOURCE_SHEET_NAME + '!' + config.GOOGLE_SOURCE_SHEET_DATA_RANGE)

        requestMonth = service.service.addZeroToMonth(config.MONTH)
        requestYear = config.YEAR
        requestProject = projectName.strip().decode('utf-8')

        fullName = ''
        outsourseList = {}
        peopleDict = dict()
        taskDict = dict()

        if outsourseInfo:
            for value in outsourseInfo[1:]:

                if value and value[0]:

                    issueId = value[0]
                    project = value[1]
                    rating = value[3]
                    summ = value[4]
                    fullName = value[5]
                    paidTime = value[8]
                    version = value[13]
                    month = service.service.addZeroToMonth(value[14].split(".")[1])
                    year = value[14].split(".")[2]

                    nameAndVersion = fullName + '-' + version

                    if month == requestMonth and year == requestYear and project == requestProject:
                        taskDict = {}

                        taskDict[issueId] = 'Rating=' + rating + '|PaidTime=' + paidTime + '|Version=' + version + '|Project=' + project

                        tempList = []
                        if nameAndVersion in outsourseList:
                            for task in outsourseList[nameAndVersion]:
                                tempList.append(task)

                        tempList.append(taskDict)

                        outsourseList[nameAndVersion] = tempList

                        self.closeAndCommentIssue(issueId)
                else:
                    print('Issue is empty')
            return outsourseList
        else:
            print('No value!')
            exit(0)

    def validateInfoInJira(self, outsourseList = []):

        def getLstFrstName(str):

            countSpace = str.count(" ")

            if countSpace == 0 or countSpace == 1:
                return str
            else:
                LstFrstName = str.split(" ")[0] + " " + outsourser.split(" ")[1]
                return LstFrstName

        validateOutsourseList = []
        count = 1
        for outsourserVersion in outsourseList:
            count += 1
            outsourser = outsourserVersion.split('-')[0]

            print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
            print ('Outsorser: ' + '\033[1m'+ outsourser  + '\033[0m' + ' version:' + outsourserVersion.split('-')[1])
            outsourserList = []

            rating = ''
            allTask = ''
            version = ''
            project = ''
            firstTask = True
            totalTime = 0
            timeProblemsNote = ''

            for tasks in outsourseList[outsourserVersion]:

                for task in tasks:
                    taskInfo = tasks[task]
                    taskTimeDocs = Decimal(service.service.getTaskInfo(taskInfo, config.Field.Time.value, True))

                    outsourserLstFrstName = getLstFrstName(outsourser)
                    taskTimeJira = Decimal(self.jiraClient.getSpendTimeOnTask(task, outsourserLstFrstName))
                    rating = service.service.getTaskInfo(taskInfo, config.Field.Rating.value)

                    version = service.service.getTaskInfo(taskInfo, config.Field.Version.value)
                    project = service.service.getTaskInfo(taskInfo, config.Field.Project.value)

                    if taskTimeJira < taskTimeDocs:
                        print ('Wrong time! JiraTime: ' + str(taskTimeJira) + ' Docstime: ' + str(taskTimeDocs))
                        timeProblemsNote += str(task) + ' Jira:' + str(taskTimeJira) + ' Docs:' + str(taskTimeDocs) + '\n'
                    else:
                        print ('Time OK! JiraTime: ' + str(taskTimeJira) + ' Docstime: ' + str(taskTimeDocs))

                    if firstTask:
                        allTask += str(task)
                    else:
                        allTask += ', ' + str(task)

                    firstTask = False

                    totalTime += taskTimeDocs

                    # Sleep
                    sleepTime = randint(1, 3)
                    sleep(sleepTime)
                    print '{: ^25}'.format('Sleep: ' + str(sleepTime))
                    print (' ')

            totalSumm = Decimal(rating.replace(',','.')) * totalTime

            # Create structure row
            outsourserList.append(allTask)
            outsourserList.append(project)
            outsourserList.append(config.STAGE_NAME.replace('PROJECT_NAME', project))
            outsourserList.append(version + '/' + config.MONTH)
            outsourserList.append(rating)
            outsourserList.append(str(totalTime))
            outsourserList.append(str(totalSumm))
            outsourserList.append(outsourser)

            #Set note
            if timeProblemsNote:
                ssID = generateReport.spreadSheetId
                range = 'A'+str(count)

                print ("Try to add a note '{0}' to the range '{1}'".format(timeProblemsNote, range))

                self.googleScriptClient.makeNote(ssID, range, timeProblemsNote)

            validateOutsourseList.append(outsourserList)

        return validateOutsourseList

    def createSourceSpreadSheets(self, currentProject):
        try:
            newSpreadSheetId = self.googleSheetClient.createSheet(config.GOOGLE_DESTINATION_SPREADSHEET_NAME + ' ' + currentProject)
            generateReport.spreadSheetId = newSpreadSheetId
            print 'Create spreadsheet with id: ' + newSpreadSheetId
        except Exception as exMessage:
            print ('Google api ex: ' + exMessage)
            print ('Continues execution is not possible!!!')
            exit(0)

    def createReport(self, validateOutsourseList):
        countUpdateCells = self.googleSheetClient.updateRangeValue(generateReport.spreadSheetId, config.GOOGLE_DESTINATION_SHEET_DATA_RANGE, validateOutsourseList)
        print ('')
        print('Count updated cells: ' + str(countUpdateCells))

    def createHead(self):
        countUpdateCells = self.googleSheetClient.updateRangeValue(generateReport.spreadSheetId, config.GOOGLE_DESTINATION_SHEET_HEAD_RANGE, config.headValues, 'COLUMNS')
        print ('')
        print('Count updated cells: ' + countUpdateCells)

    def renameWorkList(self):
        self.googleSheetClient.updateSheetName(generateReport.spreadSheetId, 0, service.service.addZeroToMonth(config.MONTH))
        print ('Sheet name update successfully to: ' + service.service.addZeroToMonth(config.MONTH))

    def closeAndCommentIssue(self, issueId):
        def checkExistComment(self, issueId, commentDate):
            comments = self.jiraClient.getIssueComment(issueId)

            for comment in comments:
                if comment.body == commentDate:
                    return True

            return False

        if config.CLOSE_AND_COMMENT_ISSUE == True:
            issueStatus = self.jiraClient.getIssueStatus(issueId)

            if issueStatus != 'Closed':
                comment = service.service.getCommentIssue(config.MONTH, config.YEAR)

                if checkExistComment(self, issueId, comment):
                    print ('Comment: ' + comment + ' already added to issue: ' + issueId)
                else:
                    self.jiraClient.addComment(issueId, comment)

                self.jiraClient.closeIssue(issueId)
            else:
                print ('Issue: ' + issueId + ' is already closed')
        else:
            print ('Closing and commenting issue ' + issueId + ' is disabled in the settings')

    def openReport(self):
        print ('Finish work!')
        spreadSheetURL = config.GOOGLE_DESTINATION_SPREADSHEET_URL.replace('id', generateReport.spreadSheetId)
        print ('Open URL: ' + spreadSheetURL)
        service.service.openLink(spreadSheetURL)

    def moveFile(self):
        print ('Try move file')
        self.googleDriveClient.moveFile(generateReport.spreadSheetId)











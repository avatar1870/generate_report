import webbrowser
import os
import requests

from decimal import Decimal

class service():
    @staticmethod
    def addZeroToMonth(month):
        intMonth = int(month)
        if intMonth >= 10:
            return str(intMonth)
        else:
            return '0' + str(intMonth)

    @staticmethod
    def getTaskInfo(taskInfo, numberSection, isNumber = False):
        value = ''

        try:
            value = taskInfo.split("|")[numberSection].split("=")[1]

            if isNumber:
                return Decimal(value.replace(',', '.'))

            return value
        except Exception as exMessage:
            print ('Parse error: ' + exMessage.message)
            return ''

    @staticmethod
    def openLink(url):
        webbrowser.open_new_tab(url)

    @staticmethod
    def read(file_path):
        with open(file_path) as f:
            return f.read()

    @staticmethod
    def getRootFolder():
        return os.path.dirname(os.path.abspath(__file__))

    @staticmethod
    def getCommentIssue(month, year):

        month = service.addZeroToMonth(month)
        lenght = len(year)

        if lenght >= 4:
            return  str(month + "/" + year[-2:])
        else:
            return str(month + "/" + year)

    @staticmethod
    def sendRequest(link, data):
        r = requests.post(link, data)
        print ('SendRequest answer' + r.text)
